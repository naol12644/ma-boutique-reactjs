import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
    return (
        <div className="sidebar">
            <div className="id">
                <div className="idContent">
                    <img src="./media/visitez-notre-boutique.png" alt="profil-pic" width="150" height="150"/>
                    <h3>Ma Boutique</h3>
                </div>
            </div>
            <div className="navigation">
                <ul>
                    <li>
                        <NavLink exact to="/" activeClassName="ravActive">
                            <i className="fas fa-home"></i>
                            <span>Accueil</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/categories" activeClassName="ravActive">
                            <i className="fas fa-images"></i>
                            <span>Catégories</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/competences" activeClassName="ravActive">
                            <i className="fas fa-mountain"></i>
                            <span>Compétences</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/contact" activeClassName="ravActive">
                            <i className="fas fa-address-book"></i>
                            <span>Contact</span>
                        </NavLink>
                    </li>
                </ul>
            </div>
            <div className="socialNetwork">
                <ul>
                    <li>
                         <a href="https://www.facebook.com/naol12644/" target="_blanck">
                            <i className="fab fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/lahcen_naoui/" target="_blanck">
                            <i className="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/lahcen_naoui/" target="_blanck">
                            <i className="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/in/lahcen-naoui-1a673683/" target="_blanck">
                            <i className="fab fa-linkedin"></i>
                        </a>
                    </li>
                </ul>
                <div className="signature">
                    <p>Développement de mon site - 2021</p>
                </div>

            </div>
        </div>
    );
};

export default Navigation;