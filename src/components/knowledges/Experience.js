import React from 'react';

const Experience = () => {
    return (
        <div className="experience">
            <h3>Expériences</h3>
            <div className="exp-1">
                <h4>Développeur full stack (Spring boot & Angular 5)</h4>
                <h5>Mai 2019 - présent </h5>
                <h4>Projets :</h4>
                <p>* JOB MANAGEMENT : Gestion des interventions</p>
                <p>* LABEL’VIE : Projet Carrefour</p>
                <p>-Outils : Spring Boot (1.5), Angular(4,5 et 7), Type Script, Bootstrap4, Html5, CSS , Jira, Git, Gitlab</p> 
                <p>* Marsa Maroc : Gestion juridiques</p>
                <p>-Outils : Java J2EE, Spring (mvc, security, data), JSF (Primes Faces).</p> 
                <h4>Formation : React JS</h4>
                <p>* Apprentissage et pratique</p>
            </div>
            <div className="exp-2">
                <h4>Prestation : Développeur Front End Angular 5</h4>
                <h5>16 juillet 2020 - 30 Avril 2021 : Freelance </h5>
                <p>Développement d’une application de gestion des fichiers de communication Interbank.</p>
            </div>
        </div>
    );
};

export default Experience;