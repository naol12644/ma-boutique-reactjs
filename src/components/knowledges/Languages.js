import React, { Component } from 'react'
import ProgressBar from './ProgressBar';

 class Languages extends Component {
     state = {
         languages: [
             {id:1, value:"Java", xp:2},
             {id:2, value:"Python", xp:0.5},
             {id:3, value:"Javascript", xp:1.8},
             {id:4, value:"Css", xp:1.5},
         ],
        framewoks:[
            {id:1, value:"Angular", xp:2},
            {id:2, value:"React JS", xp:0.3},
            {id:3, value:"Bootstrap", xp:2}
        ]

     }
    render() {
        let {languages, framewoks} = this.state;
        return (
            <div className="languagesFrameworks">
                <ProgressBar 
                    languages = {languages}
                    className="languageDisplay"
                    title="languages"
                />
                <ProgressBar 
                languages = {framewoks}
                className="frameworkDisplay"
                title="framewoks & bibliothèques"
                />
            </div>
        )
    }
}export default Languages;
