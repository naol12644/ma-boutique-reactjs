import React from 'react';

const Hobbies = () => {
    return (
        <div className="hobbies">
            <h3>Intérêts</h3>
            <ul>
                <li className="hobby">
                        <i className="fas fa-check-running"></i>
                            <span>Course à pied</span>
                    </li>
                    <li className="hobby">
                            <i className="fas fa-check-hiking"></i>
                            <span>Randonnées</span>
                    </li>
                    <li className="hobby">
                            <i className="fas fa-check-seedling"></i>
                            <span>Parmaculture</span>
                    </li>
                    <li className="hobby">
                            <i className="fas fa-check-bitcoin"></i>
                            <span>Crypto-monnaies</span>
                    </li>
                    <li className="hobby">
                            <i className="fas fa-check-rocket"></i>
                            <span>Espace</span>
                    </li>
                </ul>
        </div>
    );
};

export default Hobbies;