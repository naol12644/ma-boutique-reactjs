import React from 'react';
import Navigation from '../components/Navigation';

const Home = () => {
    return (
        <div className="home">
            <Navigation />
            <div className="homeContent">
                <div className="content">
                    <h1>Ma Boutique</h1>
                    <h2>Cherchez et achetez ce que vous voulez</h2>
                    <div className="pdf">
                        <a href="./media/CV_Lahcen_Naoui.pdf" target="_blanck">Télécharger</a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;