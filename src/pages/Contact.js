import React from 'react';
import Navigation from '../components/Navigation';
import { CopyToClipboard } from 'react-copy-to-clipboard';
const Contact = () => {
    return (
        <div className="contact">
            <Navigation />
            <div className="contactContent">
                <div className="header">
                    <div className="contactBox">
                        <h1>Contactez-nous</h1>
                        <ul>
                            <li>
                                <i className="fas fa-map-marker-alt"></i>
                                <span>Casablanca</span>
                            </li>
                            <li>
                                <i className="fas fa-mobile-alt"></i>
                                <CopyToClipboard text="0668237117">
                                    <span className="clickInput" onClick={() => {
                                        alert('Téléphone copié !');
                                    }}>
                                        06 68 23 71 17
                                    </span>
                                </CopyToClipboard>
                            </li>
                            <li>
                                <i className="fas fa-envelope"></i>
                                <CopyToClipboard text="lahcen.naoui@gmail.com">
                                    <span className="clickInput" onClick={() => {
                                        alert('E-mail copié !');
                                    }}>lahcen.naoui@gmail.com
                                    </span>
                                </CopyToClipboard>
                            </li>
                        </ul>
                    </div>
                    
                </div>
                <div className="socialNetwork">
                        <ul>
                        
                                <a href="https://www.facebook.com/naol12644/" target="_blanck">
                                    <h4>Facebook</h4>
                                    <i className="fab fa-facebook"></i>
                                </a>
                           
                                <a href="https://twitter.com/lahcen_naoui/" target="_blanck">
                                    <h4>Twitter</h4>
                                    <i className="fab fa-twitter"></i>
                                </a>
                            
                                <a href="https://www.instagram.com/lahcen_naoui/" target="_blanck">
                                    <h4>Instagram</h4>
                                    <i className="fab fa-instagram"></i>
                                </a>
                                
                                <a href="https://www.linkedin.com/in/lahcen-naoui-1a673683/" target="_blanck">
                                    <h4>Linkedin</h4>
                                    <i className="fab fa-linkedin"></i>
                                </a>
                        </ul>
                    </div>
            </div>
        </div>
    );
};

export default Contact;