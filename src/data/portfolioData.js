export const portfolioData = [
  {
    id: 1,
    name: 'S8',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/S1_8.jpeg'
  },
  {
    id: 2,
    name: 'clio 4',
    languages: ['Véhicules'],
    languagesIcons: ['fab fa-js','fab fa-react'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/clio1_4.png'
  },
  {
    id: 3,
    name: 'S8',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-php','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/S2_8.jpeg'
  },
  {
    id: 4,
    name: 'clio 4',
    languages: ['Véhicules'],
    languagesIcons: ['fab fa-js','fab fa-php','fab fa-react'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/clio2_4.jpeg'
  },
  {
    id: 5,
    name: 'repudiante 5',
    languages: ['Accessoires','Jeux et jouets'],
    languagesIcons: ['fab fa-js', 'fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/project5.JPG'
  },
  {
    id: 6,
    name: 'test 6',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/S3_8.jpeg'
  },
  {
    id: 7,
    name: 'test 7',
    languages: ['Accessoires','Jeux et jouets'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/project6.JPG'
  },
  {
    id: 8,
    name: 'test 8',
    languages: ['Accessoires','Jeux et jouets'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/project6.JPG'
  },
  {
    id: 9,
    name: 'test 9',
    languages: ['Accessoires','Jeux et jouets', 'css'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/project6.JPG'
  },
  {
    id: 10,
    name: 'S8',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/S4_8.jpeg'
  },
  {
    id: 11,
    name: 'S8',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/S4_8.jpeg'
  },
  {
    id: 12,
    name: 'A50',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/A50.jpeg'
  },
  {
    id: 13,
    name: 'A50',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/A1_50.jpeg'
  },
  {
    id: 14,
    name: 'test 14',
    languages: ['Accessoires'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/project6.JPG'
  },
  {
    id: 15,
    name: 'iphone 11',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/iphone11.jpeg'
  },
  {
    id: 16,
    name: 'iphone 12',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/iphone12.jpg'
  },
  {
    id: 17,
    name: 'iphone 12',
    languages: ['électroniques'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/iphone1_12.jpeg'
  },
  {
    id: 18,
    name: 'clio 4',
    languages: ['Véhicules'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/clio3_4.jpeg'
  },
  {
    id: 19,
    name: 'clio 4',
    languages: ['Véhicules'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/clio4_4.jpeg'
  },
  {
    id: 20,
    name: 'i30',
    languages: ['Véhicules'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/i30.jpeg'
  },
  {
    id: 21,
    name: 'i30',
    languages: ['Véhicules'],
    languagesIcons: ['fab fa-js','fab fa-react','fab fa-css3-alt'],
    source: 'http://www.github.com',
    info: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium, veritatis debitis odio eveniet quos incidunt eaque nemo eius perferendis consequatur? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus laboriosam aut nihil? Possimus, id facilis dignissimos repudiandae modi dolorum ea accusantium.',
    picture: './media/i1_30.jpeg'
  },
]