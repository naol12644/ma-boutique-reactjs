import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Home from './pages/Home';
import NotFound from './pages/NotFound';
import Contact from './pages/Contact';
import Portfolio from './pages/Portfolio';
import Knowledges from './pages/Knowledges';

const App = () => {
  return (
    <>
       <BrowserRouter> 
                      <Switch>
                          <Route path="/" exact component={Home} />
                          <Route path="/contact" component={Contact} />
                          <Route path = "/categories"  component = {Portfolio}></Route>
                          <Route path = "/competences" component = {Knowledges}></Route>
                          <Route component = {NotFound}></Route>
                      </Switch>
       </BrowserRouter>
                   
     </>
  );
};

export default App;
